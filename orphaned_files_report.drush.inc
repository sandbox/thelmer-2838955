<?php

/**
 * @file
 * Drush commands.
 */

/**
 * Implements hook_drush_command().
 */
function orphaned_files_report_drush_command() {

  $commands['orphaned-files-report'] = array(
    'description' => 'Report with orphaned files.',
    'aliases' => array('ofrep'),
    'arguments' => array(
      'max' => 'Number of orphaned files to show',
    ),
    'options' => array(
      'uri' => 'Site URL',
      'to' => 'Send report to e-mail address',
    ),
    'examples' => array(
      'drush orphaned-files-report' => 'Report with orphaned files (max 50 as default).',
      'drush orphaned-files-report all' => 'Report with all orphaned files.',
      'drush orphaned-files-report 10' => 'Report with "10" all orphaned files.',
      'drush orphaned-files-report all to=admin@example.com --uri=example.com' => 'Send report in email.',
    ),
  );

  return $commands;
}

/**
 * Implements drush_[COMMAND_NAME]().
 */
function drush_orphaned_files_report($max = 50) {
  $to = drush_get_option('to', FALSE);
  $uri = drush_get_option('uri', FALSE);
  if ($uri !== FALSE) {
    $GLOBALS['base_url'] = $uri;
  }

  $orphaned_files = orphaned_files_report_get_orphaned_files('public', $max);
  $orphaned_file_urls = array();
  foreach ($orphaned_files as $uri) {
    $orphaned_file_urls[] = file_create_url($uri);
  }

  if (!$to) {
    drush_print(implode(PHP_EOL, $orphaned_file_urls));
  }
  else {
    orphaned_files_report_send_email($to, $orphaned_file_urls);
  }
}

/**
 * Send an e-mail to a specified e-mail address.
 */
function orphaned_files_report_send_email($to, $orphaned_file_urls) {

  $from = variable_get('site_mail', 'admin@example.com');
  $body = '<html>';
  $body .= '<p>The following orphaned files were found:</p>' . PHP_EOL;
  $body .= '<table>' . PHP_EOL;
  foreach ($orphaned_file_urls as $orphan) {
    $body .= '<tr><td><a href="' . $orphan . '">' . $orphan . '</a></td></tr>';
  }
  $body .= '</table></html>';

  $message = array(
    'to' => $to,
    'subject' => t(
        'Orphaned files report from @site-name',
        array('@site-name' => variable_get('site_name', 'Drupal'))
    ),
    'body' => $body,
    'headers' => array(
      'From' => $from,
      'MIME-Version' => '1.0',
      'Content-Type' => 'text/html; charset=UTF-8;',
    ),
  );

  $system = drupal_mail_system('orphaned_files_report', 'report');

  if ($system->mail($message)) {
    drush_log(dt('E-mail message sent to <!to>', array('!to' => $to)), 'ok');
  }
  else {
    drush_set_error('DRUSH_MAIL_ERROR', dt('An error occurred while sending the e-mail message.'));
  }
}
